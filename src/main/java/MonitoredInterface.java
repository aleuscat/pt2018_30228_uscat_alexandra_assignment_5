import java.time.Duration;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface MonitoredInterface {

    List<MonitoredData> read();
    Long countDays();
    Map<String, Integer> count();
    Map<Integer,Map<String,Long>> activityCountPerDay();
    Map<String,Duration> computeDurations();
    List<String> filterActivities();
}
