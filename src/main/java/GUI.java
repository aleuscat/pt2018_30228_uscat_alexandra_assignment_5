/**
 * Made by  josh
 */


import javax.swing.*;
import java.awt.*;
import java.time.Duration;
import java.util.Map;

public class GUI extends JFrame {
    private JButton task1 = new JButton ("Task1");
    private JButton task2 = new JButton ("Task2");
    private JButton task3 = new JButton ("Task3");
    private JButton task4 = new JButton ("Task4");
    private JButton task5 = new JButton ("Task5");
    private JButton task6 = new JButton ("Task6");
    private JPanel main = new JPanel ();
    private JTextArea textArea = new JTextArea ();


    public GUI() throws HeadlessException {
        initializeComponents ();
        this.setDefaultCloseOperation (WindowConstants.EXIT_ON_CLOSE);
        this.setContentPane (this.main);
        this.pack ();
        this.setVisible (true);
        this.textArea.setEditable (false);
        MonitoredData m = new MonitoredData ();
        // add action listener for buttons
        this.task1.addActionListener (e -> {
            this.textArea.setText ("");
            for (MonitoredData mx : m.read ()) {
                this.textArea.append (mx.toString ());
                this.textArea.append ("\n");
            }
        });

        this.task2.addActionListener (e -> {
            this.textArea.setText ("Count distinct days");
            this.textArea.append (m.countDays ().toString ());
        });

        this.task3.addActionListener (e -> {
            this.textArea.setText ("Count for each activity\n");
            for (Map.Entry<String, Integer> mx : m.count ().entrySet ()) {
                this.textArea.append (mx.toString () + "\n");
            }
        });

        this.task4.addActionListener (e ->
        {
            this.textArea.setText ("Activity count for each day of the simulation period\n");
            for (Map.Entry<Integer, Map<String, Long>> mx : m.activityCountPerDay ().entrySet ()) {
                this.textArea.append ("Day" + mx.getKey () + "    ");
                this.textArea.append (mx.getValue ().toString () + "\n");
            }
        });

        this.task5.addActionListener (e -> {
            this.textArea.setText ("Total duration of the activity who has duration greater than 10 hours\n");
            for(Map.Entry<String,Duration> mx : m.computeDurations ().entrySet ()){
                this.textArea.append (mx.getKey () + "     " + mx.getValue () + "\n");
            }
        });

        this.task6.addActionListener (e -> {
            this.textArea.setText ("");
        });
    }

    private void initializeComponents() {
        this.main.setPreferredSize (new Dimension (1500, 1000));
        this.main.setLayout (new GridLayout (0, 2));
        JPanel buttonPanel = new JPanel ();
        buttonPanel.setLayout (new GridLayout (6, 0));
        buttonPanel.add (task1);
        buttonPanel.add (task2);
        buttonPanel.add (task3);
        buttonPanel.add (task4);
        buttonPanel.add (task5);
        buttonPanel.add (task6);

        JScrollPane s = new JScrollPane (this.textArea);
        //add button panel to the left side
        this.main.add (buttonPanel);
        //add text area on rigth side
        this.main.add (s);
    }
}
