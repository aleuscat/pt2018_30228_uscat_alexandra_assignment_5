import org.joda.time.Hours;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MonitoredData implements MonitoredInterface{

//start_time, end_time, activity_label)

    private Date startTime;
    private Date endTime;
    private String activity;

    private List<MonitoredData> monitoredData = new ArrayList<> ();

    public MonitoredData(Date startTime, Date endTime, String activity) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }

    public MonitoredData() {

    }


    @Override
    public String toString() {
        return "MonitoredData{" +
                "startTime=" + startTime +
                ", endTime=" + endTime +
                ", activity='" + activity + '\'' +
                '}' + "\n";
    }

    private Date setDate(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(this.startTime.getYear(),this.startTime.getMonth(),this.startTime.getDate());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.clear(Calendar.MINUTE);
        calendar.clear(Calendar.SECOND);
        calendar.clear(Calendar.MILLISECOND);
        return calendar.getTime();
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    private String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    @Override
    public List<MonitoredData> read() {
        String file = "E:\\tp5\\activities";
        DateFormat format = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
        try (Stream<String> stream = Files.lines(Paths.get(file))) {
            stream.forEach((String i) -> {
                String[] s = i.split ("\t\t");
                Date startDate = null;
                Date endDate = null;
                try{
                    startDate = format.parse (s[0]);
                    endDate = format.parse (s[1]);
                }catch (Exception e){
                    System.out.println (e.getMessage ());
                }
                MonitoredData m = new MonitoredData(startDate,endDate,s[2]);
                this.monitoredData.add(m);
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.monitoredData;
    }

    @Override
    public Long countDays() {
        List<Date> dateList = this.monitoredData.stream().map(MonitoredData::setDate).collect(Collectors.toList());
        return dateList.stream().distinct().count();
    }

    @Override
    public Map<String, Integer> count() {
        Map<String,Long> map = this.monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getActivity,Collectors.counting()));
        Map<String, Integer> map2 = map.entrySet().stream().collect(Collectors.toMap(e -> e.getKey(),e ->  (e.getValue()).intValue()));

        // write in file
        String path = "E:\\tp5\\countOfTheActivities";
        try(Writer writer = Files.newBufferedWriter(Paths.get(path))) {
            map2.forEach((key, value) -> {
                try { writer.write(key + " " + value + System.lineSeparator()); }
                catch (IOException ex) { throw new UncheckedIOException (ex); }
            });
        } catch(UncheckedIOException | IOException ex) {
            System.out.println(ex.getMessage());
        }

        return map2;
    }


    @Override
    public Map<Integer, Map<String, Long>> activityCountPerDay() {
        Map<Date, Map<String, Long>> map = this.monitoredData.stream().collect(Collectors.groupingBy(
                MonitoredData::setDate,
                Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting())
        ));

        Map<Integer,Map<String,Long>> map2 = map.entrySet().stream().collect(Collectors.toMap(e -> getDayPosition(e.getKey()),Map.Entry::getValue));

        String path = "E:\\tp5\\activityPerDay";
        try(Writer writer = Files.newBufferedWriter(Paths.get(path))) {
            map2.forEach((key, value) -> {
                try { writer.write(key + " " + value + System.lineSeparator()); }
                catch (IOException ex) { throw new UncheckedIOException(ex); }
            });
        } catch(UncheckedIOException | IOException ex) {
            System.out.println(ex.getMessage());
        }
        return map2;
    }

    @Override
    public Map<String, Duration> computeDurations() {

        Map<String, Duration> map2 = monitoredData.stream().collect(
                Collectors.groupingBy(a -> a.activity,
                        Collectors.reducing(Duration.ZERO,
                                a -> Duration.between(a.convertStartTimeToLocalDate (), a.convertEndTimeToLocalDate ()).abs(), Duration::plus
                        )
                ));

        return map2.entrySet ().stream ().filter (e -> e.getValue ().toHours () > 10).collect (Collectors.toMap (e -> e.getKey (), e-> e.getValue ()));
    }

    @Override
    public List<String> filterActivities() {
        return null;
    }

    private Integer getDayPosition(Date d){
        List<Date> dates = this.monitoredData.stream().map(MonitoredData::setDate).collect(Collectors.toList());
        List<Date> dateList = dates.stream().distinct().collect(Collectors.toList());
        return dateList.indexOf(d);
    }

    private LocalDateTime convertStartTimeToLocalDate(){
        return startTime.toInstant ().atZone (ZoneId.systemDefault ()).toLocalDateTime ();
    }

    private LocalDateTime convertEndTimeToLocalDate(){
        return endTime.toInstant ().atZone (ZoneId.systemDefault ()).toLocalDateTime ();
    }
}
